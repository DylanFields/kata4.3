//Needs a single webpage
//Display the output from each of your functions
//Each function needs to be labeled heading for each individual exercise
//All outputs for the individual katas need to be displayed using
//      - document.createElement() and associated methods
//      - store the document.createElement() into a variable called newElement
/*       
            newElement.textContent =
            JSON.stringify(variableName)
*/
//Append the output of your function to the page, each function should use
//      - return statement to return its output.
// when displaying an array, use JSON.stringify() to prettify the output.
// Look into adding a helper function that can be called within each kata function


const gotCitiesCSV = "King's Landing,Braavos,Volantis,Old Valyria,Free Cities,Qarth,Meereen";

const lotrCitiesArray = ["Mordor", "Gondor", "Rohan", "Beleriand", "Mirkwood", "Dead Marshes", "Rhun", "Harad"];

const bestThing = "The best thing about a boolean is even if you are wrong you are only off by a bit";

// Kata - 1 // 
function kata1() {
    answers("Kata 1", JSON.stringify(gotCitiesCSV.split(',')));
}

// Kata - 2 // 
function kata2() {
    answers("Kata 2", JSON.stringify(bestThing.split(' ')));
}

// kata - 3 //
function kata3() {
    let replaceCommas = gotCitiesCSV.replace(/\,/g, ';');
    answers("Kata 3", replaceCommas);
}

// kata - 4 //
function kata4() {
    let commaSeperated = lotrCitiesArray.toString();
    answers("Kata 4", commaSeperated);
}

// Kata - 5 //
function kata5() {
    let fiveCities = lotrCitiesArray.slice(0, 5);
    answers("Kata 5", fiveCities);
}

// Kata - 6 // 
function kata6() {
    let fiveCities = lotrCitiesArray.slice(-5);
    answers("Kata 6", fiveCities);
}

// Kata - 7 //
function kata7() {
    let thirdfifthcity = lotrCitiesArray.slice(2, 5);
    answers("Kata 7", thirdfifthcity);
}

// Kata - 8 //
function kata8() {
    lotrCitiesArray.splice(2, 1);
    answers("Kata 8", lotrCitiesArray);
}

// Kata - 9 //
function kata9() {
    lotrCitiesArray.splice(-2);
    answers("Kata 9", lotrCitiesArray);
}

// Kata - 10 //
function kata10() {
    lotrCitiesArray.splice(2, 0, 'Rohan');
    answers("Kata 10", lotrCitiesArray);
}

// Kata - 11 //
function kata11() {
    lotrCitiesArray.splice(5, 1, 'Deadest Marshes');
    answers("Kata 11", lotrCitiesArray);
}
// Kata - 12 //
function kata12() {
    let removedFourteenCharacters = bestThing.slice(0, 14);
    answers("Kata 12", removedFourteenCharacters);
}

// Kata - 13 //
function kata13() {
    let removeLastTwelve = bestThing.slice(-12);
    answers("Kata 13", removeLastTwelve);
}

// Kata - 14 //
function kata14() {
    let removeTwentyThirdToThirtyEight = bestThing.slice(23, 38);
    answers("Kata 14", removeTwentyThirdToThirtyEight);
}

// Kata - 15 //
function kata15() {
    let removeLastTwelveSub = bestThing.substring(69, 81);
    answers("Kata15", removeLastTwelveSub);
}

// Kata - 16 //
function kata16() {
    let removeSubTwentyThirdToThirtyEight = bestThing.substring(23, 38);
    answers("Kata 16", removeSubTwentyThirdToThirtyEight);
}

// Kata - 17 //
function kata17() {
    let leftWithOnly = bestThing.substring(64, 68);
    answers("Kata 17", leftWithOnly);
}

// Kata - 18 //
function kata18() {
    let leftWithWordBit = bestThing.indexOf('bit');
    answers("Kata 18", leftWithWordBit);
}

// Kata - 19 //
function kata19() {
    answer = gotCitiesCSV.split(',');
    let filterDoubleVowels = answer.filter(word => {
        return (
            word.includes('aa') ||
            word.includes('ee') ||
            word.includes('ii') ||
            word.includes('oo') ||
            word.includes('uu')
        )
    })
    answers("Kata 19", filterDoubleVowels);
}

// Kata - 20 //
function kata20() {
    answer = lotrCitiesArray.slice(',');
    let filterCitiesOr = answer.filter(word => {
        return (
            word.endsWith('or')
        )
    })
    answers("kata 20", filterCitiesOr);
}

// Kata - 21 //
function kata21() {
    answer = bestThing.split(' ');
    let bestThingStartWithB = answer.filter(word => {
        return (
            word.startsWith('b')
        )
    })
    answers("Kata 21", bestThingStartWithB);
}


// Kata - 22 // 
function kata22() {

    answers("Kata 22", lotrCitiesArray.includes('Mirkwood'));

}


// Kata - 23 //
function kata23() {

    answers("Kata 23", lotrCitiesArray.includes('Hollywood'));
}


// Kata - 24 //
function kata24() {
    let findIndexMirkwood = lotrCitiesArray.indexOf('Mirkwood')
    answers("Kata 24", findIndexMirkwood);
}


// Kata - 25 //
function kata25() {
    answer = lotrCitiesArray.slice(',');
    let spaceBetweenWords = answer.filter(word => {
        return (
            word.includes(' ')
        )
    })
    answers("Kata 25", spaceBetweenWords);
}


// Kata - 26 //
function kata26() {
    let reverseOrderCities = lotrCitiesArray.reverse();
    answers("Kata 26", reverseOrderCities);
}


// Kata - 27 //
function kata27() {
    let = sortAlphabetically = lotrCitiesArray.sort();
    answers("Kata 27", lotrCitiesArray);
}


// Kata - 28 //
function kata28() {
    lotrCitiesArray.sort(function(string1, string2) {
        return string1.length - string2.length
    })
    answers("Kata 28", lotrCitiesArray);
}


// Kata - 29 //
function kata29() {
    let popHarad = lotrCitiesArray.pop();
    answers("Kata 29", lotrCitiesArray);
}


// Kata - 30 //
function kata30() {
    let pushHarad = lotrCitiesArray.push('Deadest Marshes');
    answers("Kata 30", lotrCitiesArray);
}


// Kata - 31 //
function kata31() {
    let shiftMordor = lotrCitiesArray.shift();
    answers("Kata 31", lotrCitiesArray);
}


// Kata - 32 //
function kata32() {
    let unshiftMordor = lotrCitiesArray.unshift('Rhun');
    answers("Kata 32", lotrCitiesArray);
}


let kataStart = document.createElement("div");

function answers() {
    let div = document.createElement("div");
    kataStart.appendChild(div);
}


//outputs answers to HTML
function answers(title, numbers) {
    let div = document.createElement("div");
    let heading = document.createElement("h3");
    let titleText = document.createTextNode(title);
    let answerText = document.createTextNode(numbers);
    heading.appendChild(titleText);
    div.appendChild(heading);
    div.appendChild(answerText);
    kataStart.appendChild(div);
}

//outputs Kata numbers to HTML
let station = document.getElementById("content");
station.appendChild(kataStart);
kata1();
kata2();
kata3();
kata4();
kata5();
kata6();
kata7();
kata8();
kata9();
kata10();
kata11();
kata12();
kata13();
kata14();
kata15();
kata16();
kata17();
kata18();
kata19();
kata20();
kata21();
kata22();
kata23();
kata24();
kata25();
kata26();
kata27();
kata28();
kata29();
kata30();
kata31();
kata32();